"""Tests for the base_mr mixins."""
from dataclasses import asdict
from dataclasses import dataclass
from dataclasses import field
from datetime import datetime
import random
from string import ascii_letters
from unittest import TestCase
from unittest import mock

import responses
from responses.matchers import json_params_matcher

from tests.test_approval_rules import ALL_MEMBERS_RULE
from tests.test_approval_rules import X86_RULE
from tests.test_owners import OWNERS_ENTRY_1
from tests.test_owners import OWNERS_ENTRY_2
from tests.test_owners import OWNERS_HEADER
from webhook import base_mr_mixins
from webhook.defs import GITFORGE
from webhook.graphql import GitlabGraph
from webhook.owners import Parser
from webhook.pipelines import PipelineResult
from webhook.users import UserCache

API_URL = f'{GITFORGE}/api/graphql'
NAMESPACE = 'group/project'
MR_ID = 123
MR_URL = f'{GITFORGE}/{NAMESPACE}/-/merge_requests/{MR_ID}'


class TestApprovalsMixin(TestCase):
    """Tests for the ApprovalsMixin."""

    @dataclass(repr=False)
    class BasicApprovals(base_mr_mixins.ApprovalsMixin, base_mr_mixins.GraphMixin):
        """A dummy class for testing the ApprovalsMixin."""
        user_cache: UserCache
        namespace: str = NAMESPACE
        iid: int = MR_ID

    @responses.activate
    def test_approvals_rules_property(self):
        """Returns a dict of the expected ApprovalRule objects."""
        graphql = GitlabGraph()
        user_cache = UserCache(graphql, NAMESPACE)
        match_query = base_mr_mixins.ApprovalsMixin.APPROVALS_QUERY.strip('\n')

        rules_list = [ALL_MEMBERS_RULE, X86_RULE]
        json_response = {'project': {'mr': {'approvalState': {'rules': rules_list}}}}
        responses.post(API_URL, json={'data': json_response},
                       match=[json_params_matcher({'query': match_query,
                                                   'variables': {'namespace': NAMESPACE,
                                                                 'mr_id': str(MR_ID)}},
                                                  strict_match=False)])

        basic_approvals = self.BasicApprovals(graphql, user_cache)
        found_rules = basic_approvals.approval_rules

        self.assertEqual(len(found_rules), 2)
        self.assertTrue('x86' in found_rules)
        self.assertTrue('All Members' in found_rules)


class TestPipelinesMixin(TestCase):
    """Tests for the PipelinessMixin."""

    @dataclass(repr=False)
    class BasicPipelines(base_mr_mixins.PipelinesMixin, base_mr_mixins.GraphMixin):
        """A dummy class for testing the PipelinesMixin."""
        user_cache: UserCache
        namespace: str = NAMESPACE
        iid: int = MR_ID

    @responses.activate
    def test_pipelines(self):
        """Returns a dict of the expected ApprovalRule objects."""
        graphql = GitlabGraph()
        user_cache = UserCache(graphql, NAMESPACE)
        match_query = base_mr_mixins.PipelinesMixin.PIPELINES_QUERY.strip('\n')

        ds_pipe = {'id': 'gid://Gitlab/pipeline/456', 'status': 'SUCCESS'}
        job_nodes = [{'id': 123, 'name': 'c9s_merge_request', 'downstreamPipeline': ds_pipe}]
        json_response = {'project': {'mr': {'headPipeline': {'jobs': {'nodes': job_nodes}}}}}
        rsp = responses.post(API_URL, json={'data': json_response},
                             match=[json_params_matcher({'query': match_query,
                                                         'variables': {'namespace': NAMESPACE,
                                                                       'mr_id': str(MR_ID)}},
                                                        strict_match=False)])

        basic_pipelines = self.BasicPipelines(graphql, user_cache)
        found_pipes1 = basic_pipelines.pipelines
        self.assertEqual(len(found_pipes1), 1)
        self.assertTrue(isinstance(found_pipes1[0], PipelineResult))
        self.assertEqual(rsp.call_count, 1)
        # Confirm the result is cached.
        found_pipes2 = basic_pipelines.pipelines
        self.assertEqual(rsp.call_count, 1)
        self.assertIs(found_pipes1, found_pipes2)
        # Confirm fresh_pipelines is the same but not the cached value.
        found_pipes3 = basic_pipelines.fresh_pipelines
        self.assertEqual(rsp.call_count, 2)
        self.assertIsNot(found_pipes3, found_pipes2)
        self.assertEqual(found_pipes3, found_pipes2)
        self.assertEqual(len(found_pipes3), 1)
        self.assertTrue(isinstance(found_pipes3[0], PipelineResult))


class TestOwnersMixin(TestCase):
    """Tests for the OwnersMixin."""

    RH_YAML = (" - subsystem: RH Subsystem\n"
               "   labels:\n"
               "     name: redhat\n"
               "     readyForMergeDeps:\n"
               "       - testDep\n"
               "   status: Maintained\n"
               "   requiredApproval: false\n"
               "   maintainers:\n"
               "     - name: User 10\n"
               "       email: user10@redhat.com\n"
               "     - name: User 20\n"
               "       email: user20@redhat.com\n"
               "   reviewers:\n"
               "     - name: User 30\n"
               "       email: user30@redhat.com\n"
               "       restricted: true\n"
               "     - name: User 40\n"
               "       email: user40@redhat.com\n"
               "   paths:\n"
               "       includes:\n"
               "          - makefile\n"
               "          - redhat/\n")

    @dataclass(repr=False)
    class BasicOwners(base_mr_mixins.OwnersMixin):
        """A dummy class for testing the OwnerssMixin."""
        files: list = field(default_factory=list)

    @mock.patch('webhook.base_mr_mixins.get_owners_parser')
    def test_owners_mixin_unmerged(self, mock_get_owners):
        """Provides expected values for the given files list."""
        mock_get_owners.return_value = Parser(OWNERS_HEADER + self.RH_YAML + OWNERS_ENTRY_1 +
                                              OWNERS_ENTRY_2)

        params = {'owners_path': 'owners.yaml',
                  'kernel_src': '',
                  'files': ['redhat/Makefile', 'redhat/configs/rhel/generic/CONFIG_ITEM']}
        basic_owners = self.BasicOwners(**params)
        self.assertEqual(basic_owners.all_files, set(basic_owners.files))
        self.assertEqual(basic_owners.config_items, ['CONFIG_ITEM'])
        self.assertIs(basic_owners.merge_entries, False)
        self.assertEqual(basic_owners.owners, mock_get_owners.return_value)
        self.assertEqual(len(basic_owners.owners_entries), 2)

    @mock.patch('webhook.base_mr_mixins.get_owners_parser')
    def test_owners_mixin_merged(self, mock_get_owners):
        """Provides expected values for the given files list."""
        parser = Parser(OWNERS_HEADER + self.RH_YAML + OWNERS_ENTRY_1 + OWNERS_ENTRY_2)
        # delete the paths excludes so we match config items
        del parser._entries[2]._entry['paths']['excludes']
        mock_get_owners.return_value = parser

        params = {'owners_path': 'owners.yaml',
                  'kernel_src': '',
                  'files': ['redhat/Makefile', 'redhat/configs/rhel/generic/CONFIG_ITEM'],
                  'merge_entries': True}
        basic_owners = self.BasicOwners(**params)
        self.assertEqual(basic_owners.all_files, set(basic_owners.files))
        self.assertEqual(basic_owners.config_items, ['CONFIG_ITEM'])
        self.assertIs(basic_owners.merge_entries, True)
        self.assertEqual(basic_owners.owners, mock_get_owners.return_value)
        self.assertEqual(len(basic_owners.owners_entries), 1)
        entry = basic_owners.owners_entries[0]
        self.assertEqual(len(entry.maintainers), 4)
        self.assertEqual(len(entry.reviewers), 5)
        self.assertIs(entry.required_approvals, True)


class TestDiscussionsMixin(TestCase):
    """Tests for the Discussions Mixin."""

    EMPTY_NOTE = {'id': '',
                  'author': {},
                  'body': '',
                  'system': False,
                  'updatedAt': ''}

    USERNAMES = ['steve', 'vicky', 'bill', 'linus', 'mark', 'karl', 'fran']

    @dataclass(repr=False)
    class BasicDisc(base_mr_mixins.DiscussionsMixin, base_mr_mixins.GraphMixin):
        """A dummy class for testing the DiscussionsMixin."""
        user_cache: UserCache
        namespace: str = NAMESPACE
        iid: int = MR_ID

    def users(self, usernames=None):
        """Return a dict of users."""
        if not usernames:
            usernames = self.USERNAMES
        return [{'username': user,
                 'name': user.capitalize(),
                 'gid': f'gid://gitlab/User/{1 + usernames.index(user)}',
                 'email': f'{user}@example.com'} for user in usernames]

    @staticmethod
    def random_string(length=10):
        """Returns a random string of the given length."""
        return ''.join(random.choice(ascii_letters) for i in range(length))

    def new_notes(self, count=None):
        """Return a list of note dicts with random values."""
        if not count:
            count = random.randint(1, 15)
        # 2016-01-27T12:00:00 🤷
        last_note_time = datetime.fromtimestamp(1453892400)
        now_time = datetime.now()
        users = self.users()
        new_notes = []
        while count:
            count -= 1
            new_note = self.EMPTY_NOTE.copy()
            new_note['id'] = f'gid://gitlab/Note/{random.randint(1, 1000000)}'
            new_note['author'] = random.choice(users)
            new_note['body'] = self.random_string()
            last_note_time = datetime.fromtimestamp(
                                random.randint(int(last_note_time.timestamp()),
                                               int(now_time.timestamp()))
                             )
            new_note['updatedAt'] = f'{last_note_time.isoformat()}Z'
            new_notes.append(new_note)
        return new_notes

    def new_discussions(self, count=None):
        """Return a list of discussion dicts with random values."""
        if not count:
            count = random.randint(1, 50)
        new_discs = []
        while count:
            count -= 1
            resolvable = random.choice([True, False])
            resolved = random.choice([True, False]) if resolvable else False
            new_disc = {'resolvable': resolvable,
                        'resolved': resolved,
                        'notes': {'nodes': self.new_notes()}}
            new_discs.append(new_disc)
        return new_discs

    @staticmethod
    def disc_asdict(disc_obj):
        """Return the given Discussion object as a dict."""
        disc_dict = asdict(disc_obj)
        fixed_notes = []
        for note in disc_dict['notes']:
            note['author']['email'] = note['author'].pop('emails')[0]
            fixed_notes.append(note)
        disc_dict['notes'] = {'nodes': fixed_notes}
        return disc_dict

    @responses.activate
    def test_discussions_loading(self):
        """Loads all the discussions."""
        match_query = base_mr_mixins.DiscussionsMixin.DISCUSSIONS_QUERY.strip('\n')

        test_data = [self.new_discussions() for _ in range(0, 50)]
        for count, test in enumerate(test_data):
            with self.subTest(test_no=count, data=test):
                responses.reset()
                graphql = GitlabGraph()
                user_cache = UserCache(graphql, NAMESPACE)

                page_info = {'hasNextPage': False, 'endCursor': 123456}
                discs = self.new_discussions()
                json_response = {'project': {'mr': {'discussions': {'pageInfo': page_info,
                                                                    'nodes': discs}
                                                    }}}
                variables = {'namespace': NAMESPACE, 'mr_id': str(MR_ID)}
                responses.post(API_URL, json={'data': json_response},
                               match=[json_params_matcher({'query': match_query,
                                                           'variables': variables},
                                                          strict_match=False)])

                basic_disc = self.BasicDisc(graphql, user_cache)
                # Make sure the overall number of discussions matches the input.
                discussions = basic_disc.discussions
                self.assertEqual(len(discussions), len(discs))
                # Count the number of discussions per user and make sure those counts
                # match the number of discussions returned by the discussions_by_user method.
                user_discs_count = {user: len([disc for disc in discs if
                                    disc['notes']['nodes'][0]['author']['username'] == user]) for
                                    user in self.USERNAMES}
                for user, user_count in user_discs_count.items():
                    self.assertEqual(len(basic_disc.discussions_by_user(user)), user_count)
                # Count the number of system and user discussions in the input and make sure
                # the count matches the number of discussions returned by the
                # systems_discussions and user_discussions methods.
                sys_count = len([d for d in discs if d['notes']['nodes'][0]['system']])
                user_count = len([d for d in discs if not d['notes']['nodes'][0]['system']])
                self.assertEqual(len(basic_disc.system_discussions), sys_count)
                self.assertEqual(len(basic_disc.user_discussions), user_count)
                # For each discussion, finds the matching discussion.
                for disc in discs:
                    raw_note = disc['notes']['nodes'][0]
                    disc_obj = basic_disc.matching_discussion(raw_note['author']['username'],
                                                              raw_note['body'])
                    # Transform the matching object back to a dict and confirm it matches the
                    # query input.
                    disc_dict = self.disc_asdict(disc_obj)
                    self.assertEqual(disc, disc_dict)
                # Returns None when there is no matching discussion.
                self.assertIs(basic_disc.matching_discussion('marco', 'polo'), None)

    @responses.activate
    def _test_matching_discussions_loop(self):
        """Fetches a small set of discussions at a time and returns the first match."""
        match_query = base_mr_mixins.DiscussionsMixin.DISCUSSIONS_QUERY.strip('\n')
        per_page_limit = base_mr_mixins.DiscussionsMixin.QUERY_LIMIT

        # Build a list of pageInfo dicts, ensuring the last has hasNextPage = False.
        page_infos = [{'hasNextPage': True, 'endCursor': self.random_string(6)} for
                      _ in range(random.randint(3, 10))]
        page_infos[-1]['hasNextPage'] = False
        print(f'Set up {len(page_infos)} pageInfos')

        # For each pageInfo create discussions and register a response.
        raw_discs = []
        resps = []
        for index, page_info in enumerate(page_infos):
            discs = self.new_discussions(per_page_limit)
            json_response = {'project': {'mr': {'discussions': {'pageInfo': page_info,
                                                                'nodes': discs}
                                                }}}
            after = '' if index == 0 else page_infos[index-1]['endCursor']
            variables = {'namespace': NAMESPACE, 'mr_id': str(MR_ID), 'after': after,
                         'limit': per_page_limit}
            resp = responses.post(API_URL, json={'data': json_response},
                                  match=[json_params_matcher({'query': match_query,
                                                              'variables': variables},
                                                             strict_match=False)])
            raw_discs.append(discs)
            resps.append(resp)

        graphql = GitlabGraph()
        user_cache = UserCache(graphql, NAMESPACE)
        basic_disc = self.BasicDisc(graphql, user_cache)
        # For each of raw_discs lists of discussions, match each discussion using the expected
        # number of queries.
        for count, discussions in enumerate(raw_discs, start=1):
            # Confirm we find each discussion.
            for raw_disc in discussions:
                raw_note = raw_disc['notes']['nodes'][0]
                disc_obj = basic_disc.matching_discussion(raw_note['author']['username'],
                                                          raw_note['body'])
                disc_dict = self.disc_asdict(disc_obj)
                self.assertEqual(raw_disc, disc_dict)
            responses.assert_call_count(API_URL, count)

        # Confirm we have all the discussions cached.
        self.assertEqual(len(basic_disc._discussions),
                         len([disc for disc_list in raw_discs for disc in disc_list]))
        # Confirm the discussions property uses the cache.
        responses.stop()
        all_discussions = basic_disc.discussions
        self.assertEqual(all_discussions, basic_disc._discussions)

    def test_matching_discussions_loop(self):
        """Fetches a small set of discussions at a time and returns the first match."""
        for _ in range(100):
            self._test_matching_discussions_loop()
