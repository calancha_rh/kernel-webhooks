# Signoff Webhook

## Notifications

This webhook will report the status of the DCO signoff of a Merge Request in a
comment.  Any updates to that status will be delievered via edits to that
same comment.

## Manual Runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.signoff \
        --disable-inactive-branch-check \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \

The [main README][1] describes
some common environment variables that can be set that are applicable for all
webhooks.

## Reporting

- Label prefix: `Signoff::`
- Comment header: **DCO Signoff Report**

The webhook reports the overall result of the check by applying a scoped label
to the MR with the prefix `Signoff::`.

The hook will leave a comment on the MR with the details of the check. The
header of the comment is **DCO Signoff Report**. This comment will be edited
with updated information every time the hook runs. Refer to the timestamp at the
end of the comment to see when the hook last evaluated the MR.

## Triggering

To trigger reevaluation of an MR by the signoff webhook either remove any
existing `Signoff::` scoped label or a leave a comment with one of the following:

- `request-signoff-evaluation`
- `request-dco-evaluation`

Alternatively, to retrigger all the webhooks at the same time leave a note in
the MR with `request-evaluation`.

## Purpose

This hook enforces [CommitRules][2] by labeling an MR with a 'Signoff' scoped
label indicating whether the MR's *commits* and *description* all have a valid
[DCO signoff][3] line.

[1]: README.md#running-a-webhook-for-one-merge-request
[2]: https://docs.google.com/document/d/15Y8Io4N2gtvt1nn5WzfFtssiKz5vbS7YoBy81XMg9S0/#heading=h.s7aphn448iz1
[3]: https://developercertificate.org
