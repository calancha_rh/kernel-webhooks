"""Query MRs for upstream commit IDs to compare against submitted patches."""
from dataclasses import dataclass
from dataclasses import field
from os import environ
import re
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib import session
# GitPython
import git

from webhook import common
from webhook import defs
from webhook.base_mr import BaseMR
from webhook.base_mr_mixins import CommitsMixin

LOGGER = logger.get_logger('cki.webhook.fixes')
SESSION = session.get_session('cki.webhook.fixes')


@dataclass
class RHCommit:
    """Per-MR-commit data class for storing comparison data."""

    commit: set
    ucids: set = field(default_factory=set, init=False)
    fixes: str = ""


@dataclass(repr=False)
class FixesMR(CommitsMixin, BaseMR):
    """Represent the MR and possible Fixes."""

    kernel_src: str = environ.get('LINUX_SRC', '')
    ucids: set = field(default_factory=set, init=False)
    omitted: set = field(default_factory=set, init=False)

    def __post_init__(self):
        """Get the commits and basic MR data."""
        # Load the commits before the basic MR properties (super.__post_init__) because on a large
        # MR loading the commits could take some time so this way the MR state and labels are most
        # up to date once we start doing things with the data.
        self.commits  # pylint: disable=pointless-statement
        super().__post_init__()
        self.rhcommits = {}
        self.ucids = set()
        self.omitted = set()
        self.first_rhcid = 'HEAD'
        self.first_ucid = 'HEAD'
        self.fixes = {}
        self.find_upstream_commit_ids()
        self.check_for_fixes()

    def find_intentionally_omitted_fixes(self, description):
        """Add intentionally omitted fixes hashes to self.omitted set."""
        fixes_pattern = r'^\s*(?P<pfx>Omitted-fix:)[\s]+(?P<hash>[a-f0-9]{8,40})'
        fixes_re = re.compile(fixes_pattern, re.IGNORECASE)
        for line in description.split('\n'):
            gitref = fixes_re.match(line)
            if gitref:
                githash = gitref.group('hash')
                self.omitted.add(githash)
        LOGGER.debug('List of %d Omitted-fixes: %s', len(self.omitted), self.omitted)

    def map_fixes_to_commits(self, repo):
        """Map potential missing fixes to their corresponding MR commits."""
        for ucid, fixes in self.fixes.items():
            for sha, rhcommit in self.rhcommits.items():
                for fix in fixes:
                    if ucid in rhcommit.ucids:
                        rhcommit.fixes += repo.git.log("-1", "--pretty=short", fix)
                        rhcommit.fixes += f'\n    RH-Fixes: {sha[:12]} '
                        rhcommit.fixes += f'("{rhcommit.commit.title}")\n\n'

    def filter_fixes(self, mapped_fixes):
        """Filter potential fixes against included commits and Omitted-fix refs."""
        for fixee, fixes in mapped_fixes.items():
            per_commit_fixes = []
            for fix in fixes:
                propose = True
                for ucid in self.ucids:
                    if ucid.startswith(fix):
                        LOGGER.debug("Found %s in ucids", fix)
                        propose = False
                        continue
                for omit in self.omitted:
                    if omit.startswith(fix):
                        LOGGER.debug("Found %s in omitted", fix)
                        propose = False
                        continue
                if propose:
                    per_commit_fixes.append(fix)
            if per_commit_fixes:
                self.fixes[fixee] = per_commit_fixes

    def find_potential_missing_fixes(self):
        """Store potential missing upstream fixes hashes in fixes_mr.fixes mapping."""
        fixes_pattern = r'^(?P<hash>[a-f0-9]{8,40})'
        fixes_re = re.compile(fixes_pattern, re.IGNORECASE)
        mapped_fixes = {}

        repo = git.Repo(self.kernel_src)
        big_git_log = repo.git.log("--grep=Fixes:", f'{self.first_ucid}..')
        has_fixes = []
        for ucid in self.ucids:
            short_hash = ucid[:12]
            if f'Fixes: {short_hash}' in big_git_log:
                has_fixes.append(ucid)

        LOGGER.debug("Commits with Fixes: %s", has_fixes)
        for ucid in has_fixes:
            fixes = []
            short_hash = ucid[:12]
            # don't die if the "upstream" commit isn't valid (may be from ark, y-stream, etc)
            try:
                fixeslog = repo.git.log("--oneline", f'--grep=Fixes: {short_hash}',
                                        f'{short_hash}..')
            except git.GitCommandError:
                fixeslog = ""
            for line in fixeslog.split('\n'):
                gitref = fixes_re.match(line)
                if gitref:
                    githash = gitref.group('hash')
                    fixes.append(githash)
            if fixes:
                mapped_fixes[ucid] = fixes

        self.filter_fixes(mapped_fixes)
        LOGGER.debug('Map of possible Fixes: %s', self.fixes)
        self.map_fixes_to_commits(repo)

    def extract_ucid(self, commit):
        """Extract upstream commit ID from the message."""
        # pylint: disable=too-many-locals,too-many-branches,too-many-statements
        message_lines = commit.description.text.split('\n')
        gitref_list = []
        # Pattern for 'git show <commit>' (and git log) based backports
        gl_pattern = r'^(?P<pfx>commit) (?P<hash>[a-f0-9]{8,40})$'
        gitlog_re = re.compile(gl_pattern)
        # Pattern for 'git cherry-pick -x <commit>' based backports
        cp_pattern = r'^\((?P<pfx>cherry picked from commit) (?P<hash>[a-f0-9]{8,40})\)$'
        gitcherrypick_re = re.compile(cp_pattern)

        self.find_intentionally_omitted_fixes(commit.description.text)
        for line in message_lines:
            gitref = gitlog_re.match(line) or gitcherrypick_re.match(line)
            if gitref:
                githash = gitref.group('hash')
                hash_prefix = gitref.group('pfx')
                if line == f"{hash_prefix} {githash}" and len(githash) == 40:
                    if githash not in gitref_list:
                        gitref_list.append(githash)
                    self.ucids.add(githash)
                    continue
                if githash not in gitref_list:
                    gitref_list.append(githash)
                self.ucids.add(githash)

        # We return empty arrays if no commit IDs are found
        LOGGER.debug("Found upstream refs: %s", gitref_list)
        return gitref_list

    def find_upstream_commit_ids(self):
        """Check upstream commit IDs."""
        for sha, commit in self.commits.items():
            self.first_rhcid = sha
            self.rhcommits[sha] = RHCommit(commit)

        for sha, rhcommit in self.rhcommits.items():
            try:
                found_refs = self.extract_ucid(rhcommit.commit)
            # pylint: disable=broad-except
            except Exception:
                found_refs = []

            rhcommit.ucids = found_refs
            if sha == self.first_rhcid and found_refs:
                self.first_ucid = found_refs[0]

        LOGGER.debug('List of %d ucids: %s', len(self.ucids), self.ucids)

    def check_for_fixes(self):
        """Do the thing, check submitted patches vs. referenced upstream commit IDs."""
        LOGGER.debug("Checking for possible missing upstream fixes in MR %s", self.iid)
        self.find_intentionally_omitted_fixes(self.description.text)

        # do NOT run validation if the commit count is over 2000
        if len(self.commits) <= defs.MAX_COMMITS_PER_MR:
            self.find_potential_missing_fixes()

        # append quick label "Fixes" with scope "OK" or "Missing"
        if len(self.fixes) > 0:
            label_status = defs.MISSING_SUFFIX
        else:
            label_status = defs.READY_SUFFIX

        common.add_label_to_merge_request(self.gl_project, self.iid, [f'Fixes::{label_status}'])


def build_fixes_comment(fixes_mr):
    """Build a comment for possible missing Fixes commits."""
    fixes_msg = ""
    if len(fixes_mr.fixes) > 0:
        fixes_msg += "\nPossible missing Fixes detected upstream:  \n"
        fixes_msg += "```\n"
        for _, rhcommit in fixes_mr.rhcommits.items():
            fixes_msg += rhcommit.fixes
        fixes_msg += "```\n"
    else:
        fixes_msg = f"No missing upstream fixes for MR {fixes_mr.iid} found at this time.\n"

    return fixes_msg


def report_results(fixes_mr, gl_mergerequest):
    """Report the results of our examination."""
    # pylint: disable=too-many-branches
    report = build_fixes_comment(fixes_mr)
    if misc.is_production():
        common.update_webhook_comment(gl_mergerequest, fixes_mr.gl_instance.user.username,
                                      "Fixes Status", report)
    else:
        LOGGER.info('Skipping adding report in non-production\n%s', report)


def get_fixes_mr(graphql, gl_instance, projects, mr_url, **kwargs):
    """Return a merge request instance for the webhook payload."""
    params = {'graphql': graphql,
              'gl_instance': gl_instance,
              'projects': projects,
              'url': mr_url}
    if kernel_src := kwargs.get('linux_src'):
        params['kernel_src'] = kernel_src
    return FixesMR(**params)


def process_mr(gl_instance, message, graphql, projects, **kwargs):
    """Process a merge request message."""
    label_changed = common.has_label_prefix_changed(message.payload['changes'], 'Fixes::')
    if not common.mr_action_affects_commits(message) and not label_changed:
        return

    fixes_mr = get_fixes_mr(graphql, gl_instance, projects,
                            message.payload['object_attributes']['url'], **kwargs)
    gl_project = gl_instance.projects.get(message.payload["project"]["path_with_namespace"])
    gl_mergerequest = common.get_mr(gl_project, message.payload["object_attributes"]["iid"])
    report_results(fixes_mr, gl_mergerequest)


WEBHOOKS = {
    'merge_request': process_mr,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('FIXES')
    parser.add_argument('--linux-src',  **common.get_argparse_environ_opts('LINUX_SRC'),
                        help='Directory containing upstream Linux kernel git tree')
    args = parser.parse_args(args)
    if not args.linux_src:
        LOGGER.warning("No Linux source tree directory specified, using default")
    common.generic_loop(args, WEBHOOKS, linux_src=args.linux_src, get_graphql_instance=True)


if __name__ == "__main__":
    main(sys.argv[1:])
