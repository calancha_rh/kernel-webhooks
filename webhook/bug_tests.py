"""Readiness tests for Bug objects."""
# pylint: disable=invalid-name
from functools import wraps

from cki_lib.logger import get_logger

from webhook.defs import BZPriority
from webhook.defs import BZResolution
from webhook.defs import BZStatus
from webhook.defs import MrScope
from webhook.defs import MrState

LOGGER = get_logger('cki.webhook.bug_tests')


# Decorator function that runs each test.
def test_runner(passed=MrScope.READY_FOR_MERGE, failed=MrScope.NEEDS_REVIEW, keep_going=True,
                skip_if_failed=None):
    """Decorator to run one Bug Test."""
    def decorate_test(func):
        @wraps(func)
        def run_test(*args, **kwargs):
            bug = kwargs['bug'] if 'bug' in kwargs else args[0]
            f_name = func.__name__

            if skip_if_failed and any(bug.test_failed(test) for test in skip_if_failed):
                LOGGER.debug('Passing %s test as it has failed one of these tests: %s',
                             f_name, skip_if_failed)
                result = True
                result_str = 'skipped'
            else:
                result = func(*args, **kwargs)
                result_str = 'passed' if result else 'failed'
            if not result:
                bug.failed_tests.append(f_name)

            scope = passed if result else failed
            LOGGER.debug('[%s] %s %s, scope is: %s', bug.alias, f_name, result_str, scope.name)
            return result, scope, result if not keep_going else True
        return run_test
    return decorate_test


@test_runner()
def ParentCommitsMatch(bug):
    """Pass if the commits referencing the BZ in the parent MR and Dep MR match."""
    # If the dependency MR is merged then the dependant (parent) MR shouldn't have any commits which
    # reference this BZ... unless it needs to be rebased >:( ...
    if bug.mr.state is MrState.MERGED:
        return not bug.parent_mr_commits
    # ... otherwise the commits which reference the BZ should be the same!
    return bug.commits == bug.parent_mr_commits


ParentCommitsMatch.note = ("The commit SHAs referencing this BZ in this MR do not match the "
                           "commit SHAs referencing this BZ in the Dependency MR.  This indicates "
                           "this MR is based upon an older version of the Dependency MR.")


@test_runner(failed=MrScope.READY_FOR_MERGE, keep_going=False,
             skip_if_failed=['ParentCommitsMatch'])
def MRIsNotMerged(bug):
    """Pass if the Dependency Bug's MR is not merged, otherwise "Fail"."""
    # Failing this isn't bad, it just gets us a note.
    return bug.mr.state is not MrState.MERGED


MRIsNotMerged.note = "The MR associated with this Dependency BZ is already merged. Great."


@test_runner()
def InMrDescription(bug):
    """Pass if the Bug appears in the MR Description, otherwise Fail."""
    # Skip this test for any Bug that doesn't have commits such as the dummy UNTAGGED Bug.
    return bug.in_mr_description if bug.commits else True


InMrDescription.note = ("These commits have a `Bugzilla:` or `CVE:` tag which references "
                        "a BZ or CVE ID that was not listed in the merge request description.  "
                        "Please verify the tag is correct, or, add them to your MR description "
                        "as either a `Bugzilla`, `CVE`, or `Depends` tag.")


@test_runner()
def HasCommits(bug):
    """Pass if the Bug is tagged in some commits, otherwise Fail."""
    return bool(bug.commits)


HasCommits.note = ("This tag is referenced in the MR description but is not referenced in "
                   "any of the MR's commits.  Please ensure the tag is correct and update "
                   "commit descriptions with `Bugzilla`, `Depends`, or `CVE` tags as needed.")


@test_runner(failed=MrScope.INVALID, keep_going=False)
def BZisNotUnknown(bug):
    """Pass if the BZ status is not UNKNOWN, otherwise Fail."""
    # On failure sets MR scope to INVALID and stops testing.
    return bug.bz_status is not BZStatus.UNKNOWN


BZisNotUnknown.note = ("There was a problem retrieving data from Bugzilla. Please check "
                       "that the Bugzilla information is correct and contact a maintainer "
                       "if the problem persists.")


@test_runner(failed=MrScope.CLOSED, keep_going=False)
def BZisNotClosed(bug):
    """Pass if the BZ status is not CLOSED, otherwise Fail."""
    # On failure sets MR scope to INVALID and stops testing.
    return bug.bz_status is not BZStatus.CLOSED


BZisNotClosed.note = ("This BZ's status is CLOSED.  Please check that the Bugzilla "
                      "information is correct.")


@test_runner(keep_going=False)
def NotUntagged(bug):
    """Pass if the Bug is not the faux 'UNTAGGED' bug."""
    # On failure stops testing
    return not bug.untagged


NotUntagged.note = ("No `Bugzilla` tag was found in these commits.  This project requires that "
                    "each commit have at least one `Bugzilla` tag.  Please double-check the tag "
                    "formatting and/or add a `Bugzilla: <bugzilla_URL>` tag for each BZ.")


@test_runner()
def CveInMrDescription(bug):
    """Pass if all CVEs associated with the BZ are tagged in the MR Description."""
    result = True
    for cve_id in bug.bz_cves:
        if cve := next((cve for cve in bug.mr.cves if cve_id in cve.cve_ids), None):
            if cve.in_mr_description:
                continue
        result = False
        break
    return result


CveInMrDescription.note = ("This BZ is for a CVE that was not listed in the merge request "
                           "description.  Please verify the BZ's URL is correct or add the "
                           "CVE(s) to the MR description as a `CVE: CVE-YYYY-XXXXX` tag.")


# This test is Not Good because the graphql API does not expose per-commit file lists. So if an
# MR has a Bugzilla: INTERNAL tag anywhere then we can only validate whether the entire MR
# touches only internal files or not.
@test_runner()
def IsValidInternal(bug):
    """Pass if the Bug is a valid INTERNAL_BUG or not internal, otherwise Fail."""
    return bug.mr.only_internal_files if bug.internal else True


IsValidInternal.note = ("These commits are tagged as `INTERNAL` but were found to touch "
                        "source files outside of the redhat/ directory.  `INTERNAL` bugs "
                        "are only to be used for changes to files in the redhat/ directory.")


@test_runner()
def IsApproved(bug):
    """Pass if the Bug policy check passed, otherwise Fail."""
    return bool(bug.bz_policy_check_ok[0])


IsApproved.note = "The BZ associated with these commits is not approved at this time."


@test_runner(failed=MrScope.READY_FOR_QA, skip_if_failed=['IsApproved'])
def IsVerified(bug):
    """Return True if the BZ has Verified:Tested, otherwise False."""
    # Skip this test if IsApproved failed.
    return bool(bug.bz_is_verified)


IsVerified.note = ("The BZ associated with these commits has not passed preverification at "
                   "this time.")


@test_runner()
def TargetReleaseSet(bug):
    """Pass if either ITR or ZTR is set, otherwise fail."""
    # Skip this test for rhel-6 since those BZs don't use ITR/ZTR.
    if not bug.bz or bug.bz.product == 'Red Hat Enterprise Linux 6':
        return True
    return bool(bug.bz_itr or bug.bz_ztr)


TargetReleaseSet.note = "This BZ has neither ITR nor ZTR set. A BZ needs at least one set."


@test_runner(skip_if_failed=['TargetReleaseSet'])
def CentOSZStream(bug):
    """
    Pass if this is not the c9s project or the MR branch and BZ branch match.

    Fail if this is c9s and the bz_branch cannot be found or it has ZTR set.
    """
    result = True
    if bug.is_dependency or bug.mr.project.name != 'centos-stream-9' or not bug.bz:
        pass
    elif not bug.bz_branch or bug.bz_branch.zstream_target_release:
        result = False
    return result


CentOSZStream.note = ("This BZ targets a zstream release but this MR exists in the Centos "
                      "Stream project.  Centos Stream BZs are expected to target ystream.  "
                      "This MR may need to be recreated in the RHEL9 project; please "
                      "contact a maintainer for further assistance.")


@test_runner(skip_if_failed=['CentOSZStream'])
def ComponentMatches(bug):
    """Pass if the BZ component matches the MR target branch, otherwise Fail."""
    # From 9.3 onwards the bz component might be kernel or kernel-rt and that's fine.
    # Or if this bug is a cve clone then we relax this check since a single MR could address
    # CVE BZs across multiple components.
    if float(bug.mr.branch.version) >= 9.3 or bug.bz_cves:
        return bug.bz.component in ('kernel', 'kernel-automotive', 'kernel-rt') if bug.bz else True
    return bug.bz.component == bug.mr.branch.component if bug.bz else True


ComponentMatches.note = ("This BZ has a 'component' which does not match the MR's target "
                         "branch.  'kernel' BZs must have an MR which targets a kernel "
                         "branch and 'kernel-rt' BZ MRs must target a kernel-rt branch.")


@test_runner(skip_if_failed=['TargetReleaseSet', 'CentOSZStream', 'ComponentMatches'])
def BranchMatches(bug):
    """Pass if the Bug's MR Branch version matches the BZ Branch version."""
    # We can't just check if the Branch objects are the same because an MR may reference BZs
    # that have a different component than the MR Branch. So be happy as long as the version
    # numbers of the branch match.
    return bug.bz_branch.version == bug.mr.branch.version


BranchMatches.note = ("This BZ has an Internal Target Release or ZStream Target Release "
                      "which does not correspond to the target branch of this MR.  Please "
                      "review the BZ and the MR target branch to ensure they are correct.")


@test_runner()
def CvePriority(cve):
    """Pass if the CVE priority is >= high and lead stream BZ is on errata, otherwise Fail."""
    if cve.bz_priority < BZPriority.HIGH:
        return True
    parent_branch = cve.parent_mr.branch
    # Make a reversed copy of the CVE's bz_depends_on list since we want the 'highest' branch first.
    cve_clones = list(reversed(cve.bz_depends_on))
    # The lead stream clone for rhel-6 & rhel-7 is the one associated with the 'main' branch,
    # otherwise it is the first one not associated with the 'main' branch.
    if parent_branch.project.name in ('rhel-6', 'rhel-7'):
        lead_clone = next((bug for bug in cve_clones if bug.bz_branch.name == 'main'))
    else:
        lead_clone = next((bug for bug in cve_clones if bug.bz_branch.name != 'main'))
    # The clone associated with the MR.
    parent_clone = next((bug for bug in cve_clones if bug.bz_branch == parent_branch))
    if lead_clone.bz_branch > parent_clone.bz_branch and \
       lead_clone.bz_resolution != BZResolution.ERRATA:
        return False
    return True


CvePriority.note = "This CVE is not yet in errata for the lead stream."


# Tests to run on Bugzilla: Bugs
BUG_TESTS = [InMrDescription,
             HasCommits,
             BZisNotUnknown,
             BZisNotClosed,
             TargetReleaseSet,
             CveInMrDescription,
             IsValidInternal,
             IsApproved,
             IsVerified,
             CentOSZStream,
             ComponentMatches,
             BranchMatches
             ]

# Tests to run on Depends: Bugs
DEP_TESTS = [ParentCommitsMatch,
             MRIsNotMerged,
             InMrDescription,
             HasCommits,
             BZisNotUnknown,
             BZisNotClosed,
             TargetReleaseSet,
             IsValidInternal,
             IsApproved,
             IsVerified,
             CentOSZStream,
             ComponentMatches,
             ]

# Tests to run on CVE: Bugs
CVE_TESTS = [InMrDescription,
             HasCommits,
             BZisNotUnknown,
             # BZisNotClosed,
             # CvePriority
             ]

INTERNAL_TESTS = [InMrDescription,
                  HasCommits,
                  IsValidInternal]

UNTAGGED_TESTS = [NotUntagged]
