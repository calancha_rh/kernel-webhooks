"""Mixins for the BaseMR."""
from dataclasses import dataclass
from dataclasses import field
from dataclasses import replace
from functools import cached_property
from os import environ
from textwrap import dedent
from time import sleep

from cki_lib.logger import get_logger
from cki_lib.misc import get_nested_key
import prometheus_client as prometheus

from webhook.approval_rules import ApprovalRule
from webhook.common import find_config_items_kconfigs
from webhook.common import get_owners_parser
from webhook.description import Commit
from webhook.discussions import Discussion
from webhook.fragments import GL_USER
from webhook.graphql import GitlabGraph
from webhook.pipelines import PipelineResult

LOGGER = get_logger('cki.webhook.base_mr_mixins')

METRIC_KWF_QUERY_RETRIES = prometheus.Counter(
    'kwf_query_retries',
    'Number of times query data was found on an MR only after retrying'
)


@dataclass(repr=False)
class ApprovalsMixin:
    # pylint: disable=too-few-public-methods
    """A Mixin to provide support for approval rules to base_mr.BaseMR."""

    APPROVALS_QUERY_BASE = dedent("""
    query mrData($mr_id: String!, $namespace: ID!) {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          ...MrApprovals
        }
      }
    }

    fragment MrApprovals on MergeRequest {
      approvalState {
        rules {
          ...MrApprovalRule
          approved
          approvedBy {
            nodes {
              ...GlUser
            }
          }
          sourceRule {
            ...MrApprovalRule
          }
        }
      }
    }

    fragment MrApprovalRule on ApprovalRule {
      approvalsRequired
      id
      overridden
      name
      type
      eligibleApprovers {
        ...GlUser
      }
    }
    """)

    APPROVALS_QUERY = APPROVALS_QUERY_BASE + GL_USER

    @cached_property
    def approval_rules(self):
        """Return a dict of ApprovalRule objects with the rule name as the key."""
        results = self.query(self.APPROVALS_QUERY, paged_key='project/mr/approvalState/rules')
        raw_rules = get_nested_key(results, 'project/mr/approvalState/rules', [])
        return {rule['name']: ApprovalRule(self.user_cache, rule) for rule in raw_rules}


@dataclass(repr=False)
class CommitsMixin:
    """A Mixin to provide support for commits to BaseMR."""

    COMMIT_QUERY_BASE = dedent("""
    query mrData($mr_id: String!, $namespace: ID!, $after: String = "") {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          ...MrCommits
        }
      }
    }

    fragment MrCommits on MergeRequest {
      commits: commitsWithoutMergeCommits(after: $after) {
        pageInfo {
          hasNextPage
          endCursor
        }
        nodes {
          author {
            ...GlUser
          }
          authorEmail
          authorName
          authoredDate
          description
          sha
          title
        }
      }
    }
    """)

    COMMIT_QUERY = COMMIT_QUERY_BASE + GL_USER

    @property
    def all_commits(self):
        """Return a list of all the Commit objects of the MR."""
        if not self.description.text and not self.commits:
            return []
        return [self.faux_description_commit] + list(self.commits.values())

    @property
    def all_descriptions(self):
        """Return a list of all the Description objects of the MR."""
        if not self.description.text and not self.commits:
            return []
        return [self.description] + [commit.description for commit in self.commits.values()]

    @cached_property
    def commits(self):
        """Return a dict of Commit objects with sha as the key."""
        results = self.query(self.COMMIT_QUERY, paged_key='project/mr/commits')
        raw_commits = get_nested_key(results, 'project/mr/commits/nodes', [])
        commits = {}
        for raw_commit in raw_commits:
            # Pass in an author User from the cache.
            author = self.user_cache.get(raw_commit.pop('author')) if \
                raw_commit.get('author') else None
            commit = Commit(input_dict=raw_commit, author=author)
            commits[commit.sha] = commit
        return commits

    @property
    def commits_without_depends(self):
        """Return a dict of the MR commits excluding commits from depends."""
        # We use depends_mrs here because it will update self.description.depends in the case where
        # only self.description.depends_mrs is set.
        if not self.depends_mrs:
            return self.commits
        commits = {}
        for commit in self.commits.values():
            if not self.description.depends.isdisjoint(
                              commit.description.bugzilla | commit.description.jissue
                                                       ):
                break
            commits[commit.sha] = commit
        return commits

    @cached_property
    def faux_description_commit(self):
        """Return a faux Commit derived from the MR Description."""
        # If the MR author has no public email then assume (lol) the first commit is theirs and take
        # its authorEmail?
        author_email = ''
        if self.author and self.author.emails:
            author_email = self.author.emails[0]
        elif self.commits:
            author_email = list(self.commits.values())[0].authorEmail
        author_name = self.author.name if self.author else ''
        return Commit(author=self.author, authorEmail=author_email, authorName=author_name,
                      description=self.description, sha='MR Description', title=self.title)

    @property
    def first_dep_sha(self):
        """Return the sha of the first commit whose Bugzilla tag refers to a Depends."""
        if not self.description or not self.description.depends:
            return ''
        for commit in self.commits.values():
            # If any of the MRDescription.depends are in the Commit then return that commit's sha.
            if not self.description.depends.isdisjoint(
                                       commit.description.bugzilla | commit.description.jissue
                                                       ):
                return commit.sha
        return ''

    @cached_property
    def files(self):
        """Return the list of files affected by the non-dependency commits of the MR."""
        if not self.has_depends:
            return self._files
        commit_shas = list(self.commits_without_depends.keys())
        comparison = self.gl_project.repository_compare(f'{commit_shas[-1]}^', commit_shas[0])
        return [diff['new_path'] for diff in comparison['diffs']]

    @property
    def fresh_commits(self):
        """Return a fresh dict of Commits."""
        if 'commits' in self.__dict__:
            del self.commits
        return self.commits

    @property
    def has_internal(self):
        """Return True if the MR description or any commits are marked INTERNAL."""
        return any(description.marked_internal for description in self.all_descriptions)

    @property
    def has_untagged(self):
        """Return True if any commits are UNTAGGED."""
        return not all(commit.description.bugzilla or commit.description.jissue or
                       commit.description.marked_internal for
                       commit in self.commits.values())


@dataclass(repr=False)
class DependsMixin:
    """A Mixin to provide support for Dependencies to BaseMR."""

    is_dependency: bool = field(default=False, kw_only=True)

    @cached_property
    def depends_mrs(self):
        """Return a list of BaseMR objects representing any Depends: MRs."""
        return [self.new_depends_mr(mr_id) for mr_id in self.description.depends_mrs]

    def new_depends_mr(self, iid):
        """Return a new instance of self with dependency=True set."""
        new_url = f"{self.url.rsplit('/', 1)[0]}/{iid}"
        return replace(self, url=new_url, is_dependency=True)


@dataclass(repr=False)
class DiscussionsMixin:
    """A Mixin to provide support for Reviewers to base_mr.BaseMR."""

    DISCUSSIONS_QUERY_BASE = dedent("""
    query mrData($mr_id: String!, $namespace: ID!, $limit: Int!, $after: String = "") {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          ...MrDiscussions
        }
      }
    }

    fragment MrDiscussions on MergeRequest {
      discussions(first: $limit, after: $after) {
        pageInfo {
          hasNextPage
          endCursor
        }
        nodes {
          resolvable
          resolved
          notes {
            nodes {
              id
              author {
                ...GlUser
              }
              body
              system
              updatedAt
            }
          }
        }
      }
    }
    """)

    DISCUSSIONS_QUERY = DISCUSSIONS_QUERY_BASE + GL_USER

    QUERY_LIMIT = 10  # number of discussions to query at a time

    # Used by get_matching_discussion to cache results.
    _discussions: list = field(init=False, default_factory=list)
    _discs_page_info: dict = field(init=False,
                                   default_factory=lambda: {'hasNextPage': True, 'endCursor': ''})

    @cached_property
    def discussions(self):
        """Return the list of Discussions on the MR."""
        if self._discs_page_info['hasNextPage']:
            query_params = {'after': self._discs_page_info['endCursor'], 'limit': self.QUERY_LIMIT}
            results = self.query(self.DISCUSSIONS_QUERY, paged_key='project/mr/discussions',
                                 query_params_extra=query_params)
            raw_discs = get_nested_key(results, 'project/mr/discussions/nodes', [])
            new_discs = [Discussion(**r_disc, user_cache=self.user_cache) for r_disc in raw_discs]
            self._discussions.extend(new_discs)
        return self._discussions

    def discussions_by_user(self, username):
        """Return the list non-system discussions on the MR started by the given user."""
        return self._find_discussions(self.user_discussions, username)

    @staticmethod
    def _find_discussions(discussions, username, substring=None, system=False):
        """Return the list of matching Discussions."""
        if discs := [disc for disc in discussions if disc.notes[0].author.username == username and
                     disc.notes[0].system is system]:
            if substring:
                discs = [disc for disc in discs if substring in disc.notes[0].body]
        return discs

    def matching_discussion(self, username, substring, system=False):
        """Return the first Discussion by the given user with the given substring, or None."""
        # This only checks the first Note of each Discussion (thread)!
        # If we already have all or some discussions cached then search them first.
        have_all_cached = 'discussions' in self.__dict__
        if to_search := self.discussions if have_all_cached else self._discussions:
            LOGGER.debug('Searching %s %s cached discussions.',
                         'all' if have_all_cached else 'first', len(to_search))
        else:
            LOGGER.debug('No cached discussions.')
        if discs := self._find_discussions(to_search, username, substring, system):
            return discs[0]
        if have_all_cached:
            return None

        # Fetch QUERY_LIMIT discussions at a time and return the first match.
        while self._discs_page_info['hasNextPage']:
            # Do the query.
            query_params = {'after': self._discs_page_info['endCursor'], 'limit': self.QUERY_LIMIT}
            results = self.query(self.DISCUSSIONS_QUERY, query_params_extra=query_params)
            raw_discs = get_nested_key(results, 'project/mr/discussions/nodes', [])
            # Update the stored page_info.
            self._discs_page_info.update(get_nested_key(results, 'project/mr/discussions/pageInfo'))
            # Store the Discussions.
            new_discs = [Discussion(**r_disc, user_cache=self.user_cache) for r_disc in raw_discs]
            self._discussions.extend(new_discs)
            # Search the latest Discussions.
            if discs := self._find_discussions(new_discs, username, substring, system):
                return discs[0]
        return None

    @cached_property
    def system_discussions(self):
        """Return the list system discussions on the MR."""
        return [disc for disc in self.discussions if disc.notes[0].system]

    @cached_property
    def user_discussions(self):
        """Return the list non-system discussions on the MR."""
        return [disc for disc in self.discussions if not disc.notes[0].system]


@dataclass(repr=False)
class GraphMixin:
    """A mixin to provide a graphql query wrapper for MRs."""

    graphql: GitlabGraph
    query_retries: int = field(default=2, kw_only=True)
    query_retry_delay: int = field(default=5, kw_only=True)

    @staticmethod
    def __check_query_results(results):
        """Check the query results are useful."""
        if not results['project']['mr']:
            LOGGER.warning('Merge request does not exist?')
            return False
        return True

    def query(self, query_string, query_params_extra=None, paged_key=None, process_function=None):
        """Run the given query and process it with the given function."""
        query_params = {}
        if namespace := getattr(self, 'namespace'):
            query_params['namespace'] = namespace
        if iid := getattr(self, 'iid'):
            query_params['mr_id'] = str(iid)
        if query_params_extra:
            query_params.update(query_params_extra)
        for retry in range(self.query_retries):
            results = self.graphql.client.query(query_string, variable_values=query_params,
                                                paged_key=paged_key)
            if not results:
                LOGGER.warning('No results for query with params: %s\n%s', query_params,
                               query_string)
                return None
            if self.__check_query_results(results):
                if retry:
                    LOGGER.warning('API returned data on the second try 🙄.')
                    METRIC_KWF_QUERY_RETRIES.inc()
                break
            if retry:
                LOGGER.info('Still no data after retry 🤷.')
                break
            LOGGER.info('No data found, trying again in %s seconds.', self.query_retry_delay)
            sleep(self.query_retry_delay)
        return process_function(results) if process_function else results


@dataclass(repr=False, kw_only=True)
class OwnersMixin:
    """A mixin to provide support for an owners parser to base_mr.BaseMR."""

    owners_path: str = environ.get('OWNERS_YAML')
    kernel_src: str = environ.get('LINUS_SRC', '')
    merge_entries: bool = False  # whether the owners_entries method should merge results or not.

    @cached_property
    def all_files(self):
        """Return the set of all files impacted by the MR."""
        kconfigs = find_config_items_kconfigs(self.config_items, self.kernel_src) if \
            self.kernel_src else set()
        return set(self.files) | kconfigs

    @cached_property
    def config_items(self):
        """Return the list of CONFIG items touched by this MR."""
        # return common.get_configs_from_paths(self.files)
        return [path.rsplit('/', 1)[-1] for path in self.files if path.startswith('redhat/configs/')
                and '/CONFIG_' in path]

    @cached_property
    def owners(self):
        """Return an owners parser objects."""
        return get_owners_parser(self.owners_path)

    @cached_property
    def owners_entries(self):
        # pylint: disable=protected-access
        """Return the list of matching owners entries."""
        if not self.merge_entries:
            return self.owners.get_matching_entries(self.all_files)
        # It is possible for an MR to match multiple entries which have the same subsystem_label. In
        # this case we merge those entries together and just return one. Information is lost but for
        # our purposes we (currently) only care about the list of reviewers/maintainers.
        entries = {}
        for entry in self.owners.get_matching_entries(self.all_files):
            if not (existing := entries.get(entry.subsystem_label)):
                entries[entry.subsystem_label] = entry
                continue
            for user in entry.maintainers:
                if user not in existing.maintainers:
                    existing._entry['maintainers'].append(user)
            for user in entry.reviewers:
                if user not in existing.reviewers:
                    existing._entry['reviewers'].append(user)
            if entry.required_approvals:
                existing._entry['requiredApproval'] = True
        return list(entries.values())


@dataclass(repr=False)
class PipelinesMixin:
    # pylint: disable=too-few-public-methods
    """A Mixin to provide support for Pipelines to base_mr.BaseMR."""

    PIPELINES_QUERY = dedent("""
    query mrData($namespace: ID!, $mr_id: String!) {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          headPipeline {
            jobs {
              nodes {
                allowFailure
                id
                name
                createdAt
                pipeline {
                  id
                }
                status
                downstreamPipeline {
                  id
                  project {
                    id
                    fullPath
                  }
                  status
                  stages {
                    nodes {
                      name
                      jobs {
                        nodes {
                          status
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    """)

    @property
    def fresh_pipelines(self):
        """Get a fresh dict of pipelines."""
        if 'pipelines' in self.__dict__:
            del self.pipelines
        return self.pipelines

    @cached_property
    def pipelines(self):
        """Return the list of newest PipelineResults for the MR's head pipeline."""
        results = self.query(self.PIPELINES_QUERY)
        raw_pipelines = get_nested_key(results, 'project/mr/headPipeline/jobs/nodes', [])
        # If a downstream job has been retried then filter out any old results.
        return PipelineResult.prepare_pipelines(raw_pipelines)


@dataclass(repr=False)
class ReviewersMixin:
    # pylint: disable=too-few-public-methods
    """A Mixin to provide support for Reviewers to base_mr.BaseMR."""

    REVIEWERS_QUERY_BASE = dedent("""
    query mrData($mr_id: String!, $namespace: ID!) {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          ...MrReviewers
        }
      }
    }

    fragment MrReviewers on MergeRequest {
      reviewers {
        nodes {
          ...GlUser
        }
      }
    }
    """)

    REVIEWERS_QUERY = REVIEWERS_QUERY_BASE + GL_USER

    @property
    def fresh_reviewers(self):
        """Get a fresh dict of reviewers."""
        if 'reviewers' in self.__dict__:
            del self.reviewers
        return self.reviewers

    @cached_property
    def reviewers(self):
        """Return a dict of Users representing the current reviewers of the MR."""
        results = self.query(self.REVIEWERS_QUERY)
        raw_reviewers = get_nested_key(results, 'project/mr/reviewers/nodes', [])
        return {raw_reviewer['username']: self.user_cache.get(raw_reviewer) for
                raw_reviewer in raw_reviewers}

    def set_reviewers(self, usernames, mode='APPEND'):
        """Call graphql.set_mr_reviewers and uncache the reviewers property."""
        LOGGER.info('%sing reviewers with: %s', mode.capitalize().removesuffix('e'), usernames)
        self.graphql.set_mr_reviewers(self.namespace, self.iid, usernames, mode)
        if 'reviewers' in self.__dict__:
            del self.reviewers
